'use strict';

var Bids = function () {

    var defaults = {
        playersBids: {},
        isBiddingFinished: false
    };

    var bid = function (bid, player, players) {
        var index = players.indexOf(player);
        this.playersBids[index] = bid;

        if (bid == PASS) players[index].isWaitingPass = true;
    };

    var isPASS = function () {
        return this.getMaxSuit() === PASS;
    };

    var isSKIP = function (bid) {
        return bid === SKIP;
    };

    var isCompleted = function () {
        var bids = Object.values(this.playersBids);

        if (bids.indexOf(SNIP) != -1) return true;

        if (bids.length == 3) {
            if (bids.indexOf(PASS) !== -1) {
                if (count(bids, TAKE) === 2 || bids.indexOf(SKIP) !== -1) {
                    return true;
                }
            } else if (count(bids, SKIP) === 2 || count(bids, TAKE) === 2) {
                return true;
            }
        }

        return false;
    };

    var get = function (player) {
        var playersBids = Object.values(this.playersBids);

        if (!playersBids.length) {
            //Clarifying needed of the rules
            //return [PASS, SPADE, CLUB, DIAMOND, HEART, SNIP];
            return [PASS, CLUB, DIAMOND, HEART, SNIP];
        } else {
            if (player.isBidAfterTakeOut) {
                return [SPADE, CLUB, DIAMOND, HEART, SKIP, BACK];
            } else if (!intersect(playersBids, [SPADE, CLUB, DIAMOND, HEART, SNIP]).length) {
                //Clarifying needed of the rules
                //return [SPADE, CLUB, DIAMOND, HEART, SNIP, TAKE];
                return [CLUB, DIAMOND, HEART, SNIP, TAKE];
            }

            return getPossible(this.playersBids);
        }
    };

    var getPossible = function (actions) {
        var defaultBids = [PASS, SPADE, CLUB, DIAMOND, HEART, SNIP];
        var max = getMax(actions);

        return defaultBids.slice(max + 1, defaultBids.length).concat([SKIP]);
    };

    var draw = function (player, _callback) {
        var container = document.querySelector(player.container);
        var bidContainer = container.getElementsByClassName('bids')[0];
        bidContainer.innerHTML = '';

        var actions = this.get(player);
        var actionsLength = actions.length;

        for (var i = 0; i < actionsLength; i++) {
            var el = document.createElement('img');
            el.src = 'img/' + actions[i].toLowerCase() + '.png';
            el.bid = actions[i];
            el.onclick = function (e) {
                 e.stopPropagation();
                _callback(this.bid);
            };

            bidContainer.appendChild(el);
        }
    };

    var getMax = function (bids) {
        var defaultBids = [PASS, SPADE, CLUB, DIAMOND, HEART, SNIP];
        var max = 0;

        for (var i in bids) {
            var index = defaultBids.indexOf(bids[i]);
            if (index > max) {
                max = index;
            }
        }

        return max;
    };

    var getMaxBid = function () {
        var defaultBids = [PASS, SPADE, CLUB, DIAMOND, HEART, SNIP];
        var max = getMax(this.playersBids);

        return defaultBids[max];
    };

    var getMaxSuit = function () {
        var defaultBids = [PASS, SPADE, CLUB, DIAMOND, HEART, SNIP];
        var max = getMax(this.playersBids);

        return defaultBids[max] == SNIP ? SPADE : defaultBids[max];
    };

    var setBidWinner = function (players, bid) {
        var winnerIndex = 0;
        for (var i in this.playersBids) {
            if (this.playersBids[i] === bid) {
                winnerIndex = i;
            }
        }

        var player = players[winnerIndex];
        player.isBidWinner = true;
    };

    var reset = function () {
        this.playersBids = {};
        this.isBiddingFinished = false;
    };

    return extend({
        reset: reset,
        setBidWinner: setBidWinner,
        isPASS: isPASS,
        isSKIP: isSKIP,
        isCompleted: isCompleted,
        bid: bid,
        get: get,
        draw: draw,
        getMaxSuit: getMaxSuit,
        getMaxBid: getMaxBid
    }, defaults);
};