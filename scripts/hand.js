'use strict';

var Hand = function () {

    var defaults = {
        leftPlayers: [],
        isInProgress: true,
        isFirstCardMove: true,
        isTakingOutCompleted: true,
        isPassTaken: false,
        pass: [],
        currentBid: null,
        trumpSuit: null,
        activeSuit: null,
        currentStack: [],
        previousHands: [],
        leftCards: [],
        handCounter: 0
    };

    var takePass = function (players) {
        var _this = this;      
        this.isPassTaken = true;
        players.map(function (player) {
            if (player.isWaitingPass) {
                player.isBidWinner = true;
                player.isActive = true;
                player.cards = player.cards.concat(_this.pass);
                player.sortCards();
                _this.pass = [];
            } else {
                player.isActive = false;
            }
        });
    };

    var takeBid = function (players, bid) {
        this.currentBid = bid;
        players.map(function (player) {
            player.isActive = player.isInitiator;
        });
    };

    var isCompleted = function () {
        return this.handCounter == CARDS_ON_HAND_COUNT;
    };

    var isFirstMove = function () {
        return this.currentStack.length === 0;
    };

    var isSecondMove = function () {
        return this.currentStack.length === 1 && !this.leftPlayers.length;
    };

    var isLastMove = function () {
        return this.currentStack.length === 2 - this.leftPlayers.length;
    };

    var getWinnerCard = function () {
        var winner = this.currentStack[0];

        for (var i = this.currentStack.length -1; i >= 0; i--) {
            if (this.currentStack[i].card.suit == winner.card.suit) {
                if (this.currentStack[i].card.value > winner.card.value) {
                    winner = this.currentStack[i];
                }
            } else {
                if (this.currentStack[i].card.suit == this.trumpSuit) {
                    winner = this.currentStack[i];
                }
            }
        }

        return winner.card;
    };

    var getWinnerPlayer = function () {
        var winner = this.currentStack[0];

        for (var i = 1; i < 3 - this.leftPlayers.length; i++) {
            if (this.currentStack[i].card.suit == winner.card.suit) {
                if (this.currentStack[i].card.value > winner.card.value) {
                    winner = this.currentStack[i];
                }
            } else {
                if (this.currentStack[i].card.suit == this.trumpSuit) {
                    winner = this.currentStack[i];
                }
            }
        }

        this.handCounter++;
        this.currentStack = [];

        return winner.player;
    };

    var reset = function () {
        this.leftPlayers = [];
        this.isInProgress = true;
        this.isFirstCardMove = true;
        this.isTakingOutCompleted = true;
        this.isPassTaken = false;
        this.pass = [];
        this.currentBid = null;
        this.trumpSuit = null;
        this.activeSuit = null;
        this.currentStack = [];
        this.previousHands = [];
        this.leftCards = [];
        this.handCounter = 0
    };

    return extend({
        reset: reset,
        takePass: takePass,
        takeBid: takeBid,
        getWinnerPlayer: getWinnerPlayer,
        getWinnerCard: getWinnerCard,
        isCompleted: isCompleted,
        isFirstMove: isFirstMove,
        isSecondMove: isSecondMove,
        isLastMove: isLastMove
    }, defaults);
};
