var Robot = function () {

        var bid = function (bids, players, hand, playersBids) {
            var player = _find(players, 'isActive', true)[0];
            var leader = getLeader(players);
            var leaderIndex = players.indexOf(leader);
            var trump = getTrump(players, hand);

            var amILeader = (leader.id === player.id);
            var isHighScoreCritical = (leader.score > MAX_SCORE * 0.8);
            var isStrongTrumpExist = (trump.minTricks >= 6);
            var isLeaderBidIsPass = !amILeader && (playersBids[leaderIndex] === PASS);
            var isLeaderBidIsTrump = !amILeader && [SPADE, CLUB, DIAMOND, HEART].indexOf(playersBids[leaderIndex]) !== -1;
            var willLeaderWinGame = false;
            if (isLeaderBidIsTrump) {
                willLeaderWinGame = isLeaderBidIsTrump && SUITS[playersBids[leaderIndex]] * 6 + leader.score >= MAX_SCORE;
            }

            // console.log('amILeader: ', amILeader);
            // console.log('isHighScoreCritical: ', isHighScoreCritical);
            // console.log('isPassIsMine: ', isPassIsMine);
            // console.log('isStrongTrumpExist: ', isStrongTrumpExist);
            // console.log('isLeaderBidIsPass: ', isLeaderBidIsPass);
            // console.log('isLeaderBidIsTrump: ', isLeaderBidIsTrump);
            // console.log('willLeaderWinGame: ', willLeaderWinGame);

            // TODO: if somebosies trump ok for you
            if (!amILeader && isHighScoreCritical) {
                //Snip if game will ends
                if (willLeaderWinGame) {
                    if (bids.indexOf(SNIP) !== -1) {
                        return bids[bids.indexOf(SNIP)];
                    }
                }
                //Take pass expecting loose
                if (isLeaderBidIsPass && trump.minTricks < 7) {
                    if (bids.indexOf(TAKE) !== -1) {
                        return bids[bids.indexOf(TAKE)];
                    }
                }

                // Leaver leader trump, expecting loose
                if (isLeaderBidIsTrump) {
                    var trumpCards = getSuitCards(player.cards, playersBids[leaderIndex]);
                    if (trumpCards.length >= 3 && trump.minTricks >= 4) {
                        if (bids.indexOf(SKIP) !== -1) {
                            return bids[bids.indexOf(SKIP)];
                        }
                    }
                }
            }

            if (isStrongTrumpExist) {
                if (bids.indexOf(trump.suit) !== -1) {
                    player.currentTrump = trump;
                    player.goal = trump.minTricks;
                    return bids[bids.indexOf(trump.suit)];
                }
            } else if (player.isInitiator) {
                if (hand.isTakingOutCompleted && !isHighScoreCritical && [SPADE, CLUB].indexOf(trump.suit) !== -1) {
                    if (getSuitCards(player.cards, trump.suit).length > 2 && ((trump.suit === SPADE && trump.minTricks >= 4) || (trump.suit === CLUB && trump.minTricks >= 5))) {
                        if (bids.indexOf(trump.suit) !== -1) {
                            player.currentTrump = trump;
                            player.goal = trump.minTricks > 6 ? trump.minTricks : 6;
                            return bids[bids.indexOf(trump.suit)];
                        }
                    }
                }
            }

            if (bids.indexOf(PASS) !== -1) {
                return bids[bids.indexOf(PASS)];
            }

            if (bids.indexOf(TAKE) !== -1) {
                return bids[bids.indexOf(TAKE)];
            }

            if (bids.indexOf(SKIP) !== -1) {
                return bids[bids.indexOf(SKIP)];
            }
        };

        var getFirstMoveCard = function (players, hand) {
            var player = _find(players, 'isActive', true)[0];
            var rootState = new PastonState(players, hand);

            var isTrumpCleared = isSuitCleared(hand.trumpSuit, player.cards, hand.leftCards, hand.pass);
            var isPossibleToClearTrump = isPossibleToClearSuit(player.cards, hand.leftCards, hand.trumpSuit, hand.leftPlayers);
            var winnerCard = getWinnerCard(player.cards, hand);
            var trumpCards = getSuitCards(player.cards, hand.trumpSuit);

            if (isPossibleToClearTrump && !isTrumpCleared) {
                return getMaxCard(trumpCards);
            }

            if (player.isBidWinner) {
                if (isTrumpCleared) {
                    if (winnerCard) {
                        return winnerCard;
                    }
                } else if (trumpCards.length > 3) {
                    return getMaxCard(trumpCards);
                } else {
                    if (winnerCard) {
                        return winnerCard;
                    }
                }
            }

            return ISMCTS(rootState, MAX_SIMULATIONS, true).card;

        };

        var getSecondMoveCard = function (players, hand) {
            var rootState = new PastonState(players, hand);
            var player = _find(players, 'isActive', true)[0];
            var playedCard = hand.currentStack[0].card,
                cards, maxCard, outsideMax;

            if (hand.activeSuit !== hand.trumpSuit && suitExists(player.cards, hand.activeSuit)) {
                return ISMCTS(rootState, MAX_SIMULATIONS, true).card;

            } else if (suitExists(player.cards, hand.trumpSuit)) {
                cards = getSuitCards(player.cards, hand.trumpSuit);
                var outsideTrumps = getOutSideCards(cards, getSuitCards(hand.leftCards, hand.trumpSuit), createSuitCards(hand.trumpSuit));

                if (outsideTrumps.length) {
                    outsideMax = getMaxCard(outsideTrumps);
                    maxCard = getMinWinner(cards, outsideMax);

                    if (maxCard && maxCard.value > playedCard.value) {
                        return maxCard;
                    } else {
                        return getMinCard(cards);
                    }
                } else {
                    return getMinCard(cards);
                }
            } else {
                return getMinCard(player.cards);
            }
        };

        var getLastMoveCard = function (players, hand) {
            var player = _find(players, 'isActive', true)[0];
            var cards, maxCard, minCard;
            var winnerCard = hand.getWinnerCard();

            if (hand.activeSuit !== hand.trumpSuit && suitExists(player.cards, hand.activeSuit)) {
                cards = getSuitCards(player.cards, hand.activeSuit);
                if (winnerCard.suit === hand.trumpSuit) {
                    return getMinCard(cards);
                } else {
                    maxCard = getMinWinner(cards, winnerCard);
                    minCard = getMinCard(cards);

                    return (maxCard) ? maxCard : minCard;
                }
            } else if (suitExists(player.cards, hand.trumpSuit)) {
                cards = getSuitCards(player.cards, hand.trumpSuit);

                if (winnerCard.suit === hand.trumpSuit) {
                    maxCard = getMinWinner(cards, winnerCard);
                    minCard = getMinCard(cards);

                    return (maxCard) ? maxCard : minCard;

                } else {
                    return getMinCard(cards);
                }
            } else {
                return getMinCard(player.cards);
            }
        };

        var move = function (players, hand) {
            var player = _find(players, 'isActive', true)[0];
            if (player.cards.length === 1) {
                return player.cards[0];
            }

            player.goal = updateGoal(player, hand);
            console.log("GOAL: ", player.goal);

            //return ISMCTS(rootState, MAX_SIMULATIONS, true).card;

            if (hand.isFirstMove()) {
                return getFirstMoveCard(players, hand);
            } else {
                if (hand.isSecondMove()) {
                    return getSecondMoveCard(players, hand);
                } else if (hand.isLastMove()) {
                    return getLastMoveCard(players, hand);
                }
            }
        };

        var isPlaying = function (players, hand, playersBids) {
            var player = _find(players, 'isActive', true)[0];
            var bidWinner = _find(players, 'isBidWinner', true)[0];
            var leader = getLeader(players);

            var amILeader = (leader.id === player.id);
            var isHighScoreCritical = (!amILeader && leader.id === bidWinner.id && leader.score > MAX_SCORE * 0.8);

            if(!isHighScoreCritical) {
                var ranking = getSuitsRanking(player.cards);
                var minTricks = 0;

                for (var suit in ranking) {
                    if(suit !== hand.trumpSuit) {
                        minTricks += ranking[suit].ifNotTrump;
                    } else {
                        minTricks += ranking[suit].maxTricks;;
                    }
                }

                return minTricks >= 2;
            }

            return true;
        };

        var takeOutPass = function (player) {
            var max = {
                count: 0,
                card1: null,
                card2: null
            };

            for (var i = 0; i < player.cards.length; i++) {
                for (var j = i + 1; j < player.cards.length; j++) {
                    var tempCards = player.cards.slice();

                    var card1 = player.cards[i];
                    var card2 = player.cards[j];

                    tempCards.splice(tempCards.indexOf(card1), 1);
                    tempCards.splice(tempCards.indexOf(card2), 1);

                    var ranking = getSuitsRanking(tempCards);
                    var minTricks = 0;
                    var maxTricks = 0;

                    for (var suit in ranking) {
                        minTricks += ranking[suit].minTricks;
                        maxTricks += ranking[suit].maxTricks;

                    }

                    if (max.count < maxTricks) {
                        max = {
                            count: minTricks,
                            card1: card1,
                            card2: card2
                        };
                    }
                }
            }

            if (max.card1 && max.card2) {
                console.log(max);
                return [
                    player.cards[player.cards.indexOf(max.card1)],
                    player.cards[player.cards.indexOf(max.card2)]
                ];
            }

            return player.cards.slice(0, 2);
        };

        return {
            move: move,
            bid: bid,
            isPlaying: isPlaying,
            takeOutPass: takeOutPass
        };
    }
    ;