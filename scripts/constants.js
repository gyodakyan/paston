'use strict';
var SPADE = 'SPADE',
    CLUB = 'CLUB',
    DIAMOND = 'DIAMOND',
    HEART = 'HEART';

var PASS = 'PASS',
    SNIP = 'SNIP',
    SKIP = 'SKIP',
    TAKE = 'TAKE',
    BACK = 'BACK';

var CARD_NAMES = ['7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

var SUITS = {
    SPADE: 1,
    CLUB: 2,
    DIAMOND: 3,
    HEART: 4
};

var MAX_SIMULATIONS = 2500;
var MAX_SCORE = 121;
var CARDS_ON_HAND_COUNT = 10;
var HAND_AUTOLOAD_DURATION = 3000;
var HAND_LOAD_DURATION = 1300;
var ROBOT_BID_DURATION = 1000;
var ROBOT_CARD_PLAY_DURATION = 700;
var HAND_CALCULATE_DURATION = 1000;
var HAND_AUTOCALCULATE_DURATION = 2000;

var SUCCESS = 'background: green; color: #fff';
var WARNING = 'background: #fba710; color: #fff';
var ERROR = 'background: red; color: #fff';
var INFO = 'background: #0490fd; color: #fff';
var PRIMARY = 'background: #bfc3c5; color: #fff';
var BLACK = 'background: #000; color: #fff';