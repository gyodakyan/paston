'use strict';

/**
 * Main Game Logic
 * @type {}
 */
var Game = {

    initialize: function () {
        this.registerHandlers();
        this.widgets = new Widgets();
        this.players = createPlayers();
        this.hand = new Hand();
        this.bids = new Bids();

        this.robot = new Robot();
        this.deck = new Deck;
        this.newHand();
    },

    registerHandlers: function () {
        document.body.addEventListener('click', this.clickHandler.bind(this));
    },

    clickHandler: function () {
        if (this.cardsPlayed) {
            this.widgets.clearCards();
            this.widgets.updateHandCounts(this.players);
            this.player.isActive = true;
            this.cardsPlayed = false;
            this.move();
        }

        if (this.hand.isInProgress) return;
        this.newHand();
    },

    requestNewHand: function () {
        var _this = this;
        _this.hand.isInProgress = false;
        _this.widgets.updateHandCounts(_this.players);
        _this.widgets.clearCards();
        _this.widgets.displayResults(this.players, this.hand);
        _this.widgets.updateScores(this.players);
        _this.displayStartingCards();
        // delay(7000).then(function(){
        //     _this.newHand();
        // });

    },

    newHand: function () {
        var _this = this;
        _this.reset();
        _this.widgets.hideResults();
        _this.widgets.updateHandCounts(_this.players);

        delay(HAND_LOAD_DURATION).then(function () {
            _this.deal();
            _this.displayPlayers();
            _this.setInitiator();
            _this.player = _this.getActivePlayer();
            //console.log('%c NEW HAND STARTS! INITIATOR IS: ' + _find(_this.players, 'isInitiator', true)[0].name || "UNDEFINED", SUCCESS);
            _this.waitBid();

        });
    },

    reset: function () {
        this.hand.reset();
        this.bids.reset();
        this.players.map(function (player) {
            player.reset();
        });
    },

    deal: function () {
        var deck = this.deck.shuffle();
        this.players.map(function (player, index) {
            player.receiveCards(deck.cards.slice(CARDS_ON_HAND_COUNT * (index), CARDS_ON_HAND_COUNT * (index + 1)));
        });
        this.hand.pass = this.deck.cards.slice(30, 32);
    },

    displayStartingCards: function () {
        this.players.map(function (player) {
            player.displayStartingCards();
        });
    },

    displayPlayers: function () {
        var _this = this;
        this.players.map(function (player) {
            player.display(function (card) {
                _this.cardClicked(card);
            }, false);
        });
    },

    hideBids: function () {
        this.players.map(function (player) {
            if (!player.isBidWinner) {
                player.hideBid();
            }
        });
    },

    setInitiator: function () {
       // this.dealer = 2;
        if (typeof this.dealer === 'undefined') {
            var c = ['9S', 'JS', 'QS'];
            this.hand.pass.forEach(function (p) {
                if (c.indexOf(p.cid) !== -1) {
                    c.splice(c.indexOf(p.cid), 1);
                }
            });
            for (var i = 0; i < 3; i++) {
                var isFound = _find(this.players[i].cards, 'cid', c[0]);
                if (isFound.length) {
                    this.dealer = i;
                    break;
                }
                else {
                    this.dealer = 0;
                }
            }
        } else if (typeof this.players[++this.dealer] === 'undefined') {
            this.dealer = 0;
        }

        this.players[this.dealer].isActive = true;
        this.players[this.dealer].isInitiator = true;
    },

    waitBid: function () {
        //console.log('%c WAITING BID FROM: ' + this.player.name, WARNING);
        var _this = this;
        this.player.displayActiveState();

        if (this.player.isWaitingPass) {
            //console.log('%c AS LAST BID IS PASS, THIS PLAYER IS NOT ALLOWED TO BID: ' + this.player.name, ERROR);
            this.setNextActivePlayer();
            this.waitBid();
            return;
        }

        if (this.player.type === 'robot') {
            var bids = this.bids.get(this.player);           
            delay(ROBOT_BID_DURATION).then(function () {
                var bid = _this.robot.bid(bids, _this.players, _this.hand, _this.bids.playersBids);
                //console.log(_this.player.name, '  ', bid);
                _this.bid(bid);
            });
        } else {
            this.bids.draw(this.player, this.bid.bind(this));
        }
    },

    bid: function (bid) {
        if (bid === BACK) {
            this.player.isWaitingPass = true;

            this.hand.takePass(this.players);
            this.hand.isTakingOutCompleted = false;
            this.displayPlayers();
            //console.log(this.hand);
            this.move();
            return;
        }
        this.player.hideActiveState();
        this.player.hideBids();
        //console.log('%c ' + this.player.name + ' BID IS: ' + bid, INFO);
        this.bids.bid(bid, this.player, this.players);
        //Animation goes here

        switch (bid) {
            case SKIP:
                this.player.displayBid("PASS", true);
                break;
            case TAKE:
                this.player.displayBid(TAKE, true);
                break;
            default:
                this.player.displayBid(bid);
        }

        if (this.bids.isCompleted()) {
            if (this.bids.isPASS()) {
                if (!this.hand.isPassTaken) {
                    this.hand.takePass(this.players);
                    this.hand.isTakingOutCompleted = false;

                    this.player = this.getActivePlayer();
                    this.player.displayActiveState();

                    if (this.player.type !== 'person') {
                        var cards = this.robot.takeOutPass(this.player, this.hand);
                        //console.log('ROBOT PASS CARDS: ', cards);
                        this.cardClicked(cards[0]);
                        this.cardClicked(cards[1]);
                    }
                    this.displayPlayers();
                    return;
                } else {
                    if (this.bids.isSKIP(bid)) {
                        this.looseHand();
                        return;
                    }
                }
            } else {
                this.hand.takeBid(this.players, this.bids.getMaxBid());
                this.hand.trumpSuit = this.bids.getMaxSuit();
                this.bids.isBiddingFinished = true;
                this.bids.setBidWinner(this.players, this.hand.currentBid);
                this.player = this.getActivePlayer();
            }
            //console.log('%c BIDDING COMPLETED: BID WINNER IS: ' + _find(this.players, 'isBidWinner', true)[0].name + ' TRUMP IS: ' + this.hand.trumpSuit + ' BID IS: ' + this.hand.currentBid, BLACK);
            this.move();
        } else {
            this.setNextActivePlayer();
            this.waitBid();
        }
    },

    move: function () {
        var player = this.player;
        if (this.hand.isFirstCardMove) {
            this.hand.isFirstCardMove = false;
        }

        //console.log('%c WAITING CARD FROM: ' + player.name, INFO);

        if (this.hand.leftPlayers.length == 2) {
            this.winHand();
            return false;
        }

        if (this.hand.currentStack.length == this.players.length - this.hand.leftPlayers.length) {
            this.calculateHand(player);
            return false;
        }

        if (this.hand.leftPlayers.indexOf(player) !== -1) {
            this.setNextActivePlayer();
            this.move();
            return false;
        }

        this.player.displayActiveState();

        if (player.type == 'person') {
            if (!player.isBidWinner && player.isFirstMove) {
                player.displaySkipButton(this.leaveHand.bind(this));
                return false;
            } else {
                player.hideSkipButton();
            }
        } else {
            if (player.isFirstMove) {
                if (!this.robot.isPlaying(this.players, this.hand, this.bids.playersBids)) {
                    this.leaveHand();
                    return false;
                }
            }
            var hand = this.hand;
            var players = [];
            this.players.forEach(function (player) {
                if (hand.leftPlayers.indexOf(player) === -1) {
                    players.push(player);
                }
            });
            var card = this.robot.move(players, hand);

            var _this = this;
            delay(ROBOT_CARD_PLAY_DURATION).then(function () {
                _this.cardClicked(card);
            });
        }
    },

    validateCard: function (card, player) {
        if (!player.hasCard(card)) return false;

        if (this.hand.activeSuit) {
            if (card.suit != this.hand.activeSuit) {
                if (player.hasSuit(this.hand.activeSuit)) return false;
                if (card.suit != this.hand.trumpSuit && player.hasSuit(this.hand.trumpSuit)) return false;
            }
        }

        return true;
    },

    cardClicked: function (card) {
        if (!this.player.isActive) return;
        if (!this.bids.isBiddingFinished && this.hand.isTakingOutCompleted) return;

        var player = this.player;

        if (player.isFirstMove) {
            if (!player.isBidWinner) {
                player.displayBid('WHIST', true);
            }
        }
        player.isFirstMove = false;
        player.skipCount = 0;

        for (var i = this.hand.currentStack.length - 1; i >= 0; i--) {
            if (this.hand.currentStack[i].player.id === player.id) {
                return false;
            }
        }

        //console.log('%c CARD: ' + card.cid + " FROM " + player.name, PRIMARY);

        if (this.bids.isPASS() && !this.hand.isTakingOutCompleted) {
            if (!player.hasCard(card)) return false;
            this.hand.pass.push(card);
            player.cards.splice(player.cards.indexOf(card), 1);
            card.remove();

            if (this.hand.pass.length == 2) {
                this.hand.isTakingOutCompleted = true;
                player.isWaitingPass = false;
                player.isBidAfterTakeOut = true;
                this.waitBid();
            }
        } else {
            if (!this.hand.currentStack.length) {
                this.hand.activeSuit = card.suit;
            }

            if (this.validateCard(card, player)) {
                this.player.hideActiveState();
                this.player.hideSkipButton();

                card.move(player.orientation);
                player.cards.splice(player.cards.indexOf(card), 1);
                this.hand.currentStack.push({player: player, card: card});
                this.hand.leftCards.push(card);

                if (this.hand.currentStack.length == this.players.length - this.hand.leftPlayers.length) {
                    this.hand.previousHands =  this.hand.previousHands.concat( this.hand.currentStack);
                    this.calculateHand(player);
                } else {

                    this.setNextActivePlayer();
                    this.move();
                }
            }
        }
    },

    /**
     * This function works when both players leave the hand
     * In this case user automatically win the hand
     */
    winHand: function () {
        var player = this.getBidWinnerPlayer();
        player.currentScore += 10 * SUITS[this.hand.trumpSuit];
        player.score += player.currentScore;

        // Check is there player who leave hand 3 time
        this.hand.leftPlayers.map(function (pl) {
            if (pl.skipCount === 3) {
                pl.score += -10;
            }
        });

        this.requestNewHand();
    },

    /**
     * This function works when user click leave button and don`t play any card
     * Leave button visible only on first card move
     */
    leaveHand: function () {
        //console.log('%c PLAYER LEFT HAND: ' + this.player.name, ERROR);
        this.hand.leftPlayers.push(this.player);
        this.player.skipCount++;
        this.player.hideSkipButton();
        this.player.hideActiveState();
        this.player.displayLeftState();

        this.setNextActivePlayer();
        this.move();
    },

    /**
     * This function works when user click skip hand button
     */
    looseHand: function () {
        var initiator = this.getBidWinnerPlayer();
        if(!initiator) {
            initiator = _fund(this.players, 'isWaitingPass', true)[0];
        }
        initiator.currentScore = -10;
        initiator.score += -10;
        this.widgets.clearCards();
        this.requestNewHand();
    },

    calculateHand: function (player) {
        var _this = this;
        _this.cardsPlayed = false;
        player.isActive = false;
        _this.player = _this.hand.getWinnerPlayer();
        _this.player.currentHandsCount++;
        var isGameOver = false;
        //console.log('%c HAND WINNER: ' + _this.player.name, PRIMARY);
        if (_this.hand.isCompleted()) {
            delay(HAND_CALCULATE_DURATION).then(function () {
                _this.players.map(function (player) {
                    if (_this.hand.leftPlayers.indexOf(player) != -1) {
                        if (player.skipCount === 3) {
                            player.score += -10;
                        }
                        return;
                    }
                    var handsCount = (player.isBidWinner) ? 6 : 2;

                    if (player.currentHandsCount < handsCount) {
                        if (_this.hand.currentBid == SNIP) {
                            if (player.isBidWinner) {
                                player.currentScore += -40;
                            } else {
                                player.currentScore += -10;
                            }

                        } else {
                            player.currentScore += -10 * SUITS[_this.hand.trumpSuit];
                        }

                    } else {
                        player.currentScore += player.currentHandsCount * SUITS[_this.hand.trumpSuit];
                    }

                    player.score += player.currentScore;

                    if (player.score >= MAX_SCORE) {
                        isGameOver = true;
                           delay(1000).then(function () {
                                _this.widgets.displayGameOver(player, _this.requestNewHand);
                           });
                        
                    }
                    //console.log('%c HANDS COUNT: ' + player.currentHandsCount + ', TOTAL SCORE: ' + player.score, PRIMARY);
                });
               
                if(!isGameOver) {
                    _this.requestNewHand();
                }
                
            });
        } else {
            _this.cardsPlayed = true;
            delay(HAND_AUTOCALCULATE_DURATION).then(function () {
                if (!_this.cardsPlayed) return;
                _this.widgets.clearCards();
                _this.widgets.updateHandCounts(_this.players);
                _this.player.isActive = true;
                _this.move();
            });
        }
    },

    getBidWinnerPlayer: function () {
        return _find(this.players, 'isBidWinner', true)[0] || false;
    },

    getActivePlayer: function () {
        return _find(this.players, 'isActive', true)[0] || false;
    },

    setNextActivePlayer: function () {
        var index = this.players.indexOf(this.player);
        this.player.isActive = false;

        this.player = (typeof this.players[++index] !== 'undefined') ? this.players[index] : this.players[0];
        this.player.isActive = true;

        return this.player;
    },

    getSavedData: function () {
        var data = null;
    },
};

Game.initialize();