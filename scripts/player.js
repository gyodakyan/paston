'use strict';

var Player = function (name, type, container, orientation) {
    this.id = guid();
    this.name = name;
    this.type = type;
    this.container = container;
    this.orientation = orientation;
    this.isActive = false;
    this.isInitiator = false;
    this.isWaitingPass = false;
    this.isBidAfterTakeOut = false;
    this.isFirstMove = true;
    this.isBidWinner = false;
    this.currentHandsCount = 0;
    this.score = 0;
    this.currentScore = 0;
    this.skipCount = 0;
    this.cards = [];
    this.cardsCopy = [];
    //
    this.currentTrump = {},

    this.goal = 0;


};

Player.prototype.displayStartingCards = function () {
    var container = document.querySelector(this.container);
    var cardsContainer = container.getElementsByClassName('cards')[0];
    cardsContainer.innerHTML = '';

    this.cardsCopy.forEach(function (card) {
        card.draw(cardsContainer, true, 70);
    });
};

Player.prototype.display = function (_callback) {
    var container = document.querySelector(this.container),
        type = this.type,
        onclick = null,
        face = false; //change to false on production

    var cardsContainer = container.getElementsByClassName('cards')[0];
    cardsContainer.innerHTML = '';

    var name = container.getElementsByClassName('name')[0];
    name.innerHTML = this.name;

    var cardWidth;

    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

    if (this.orientation !== 'middle') {
        cardWidth = w / (2.3 * 10);
        cardsContainer.style.width = (cardWidth - 25) * 10 + 'px';
    } else {
        cardWidth = w / (15);
    }

    this.cards.forEach(function (card) {
        if (type == 'person') {
            onclick = function (e) {
                e.stopPropagation();
                _callback(card);
            };
            face = true;
        }

        card.draw(cardsContainer, face, cardWidth);
        card.img.onclick = onclick;
    });
};

Player.prototype.displaySkipButton = function (callback) {
    var playerContainer = document.querySelector(this.container);
    var container = playerContainer.getElementsByClassName('bids')[0];
    var skipBtn = document.createElement('img');
    skipBtn.src = 'img/skip.png';
    skipBtn.onclick = function (e) {
        e.stopPropagation();
        callback();
    };
    container.appendChild(skipBtn);
};

Player.prototype.hideSkipButton = function () {
    var playerContainer = document.querySelector(this.container);
    var container = playerContainer.getElementsByClassName('bids')[0];
    container.innerHTML = '';
};

Player.prototype.hideBid = function () {
    var container = document.querySelector(this.container);
    var bidContainer = container.getElementsByClassName('bid')[0];

    if (bidContainer)
        bidContainer.remove();
};

Player.prototype.hideBids = function () {
    var container = document.querySelector(this.container);
    var bidContainer = container.getElementsByClassName('bids')[0];
    bidContainer.innerHTML = '';
};

Player.prototype.displayBid = function (bid, isText) {
    var container = document.querySelector(this.container);
    var bidElement;
    if (container.getElementsByClassName('bid')[0]) {
        container.getElementsByClassName('bid')[0].remove();
    }
    var bidContainer = document.createElement('div');
    bidContainer.className = 'bid';

    var bidPosition = container.getElementsByClassName('avatar')[0].getBoundingClientRect();

    if (this.orientation === 'right') {
        bidContainer.style.right = bidPosition.width * 3 + 'px';
    } else {
        bidContainer.style.left = bidPosition.width * 3 + 'px';
    }

    if (isText) {
        bidElement = document.createElement('span');
        bidElement.innerHTML = bid;
    } else {
        bidElement = document.createElement('img');
        bidElement.src = 'img/' + bid.toLowerCase() + '.png';
    }

    bidContainer.appendChild(bidElement);
    container.appendChild(bidContainer);
};

Player.prototype.receiveCards = function (cards) {
    this.cards = cards;
    this.sortCards();
    this.cardsCopy = this.cards.slice();
};

Player.prototype.sortCards = function () {
    this.cards = this.cards.sort(function (a, b) {
        if (a.suit === b.suit) {
            return ((a.value < b.value) ? -1 : (a.value > b.value) ? 1 : 0) * -1;
        } else {
            return ((SUITS[a.suit] < SUITS[b.suit]) ? -1 : (SUITS[a.suit] > SUITS[b.suit]) ? 1 : 0) * -1;
        }
    });
};

Player.prototype.displayLeftState = function () {
    this.displayBid('I AM NOT PLAYING!', true);
    var container = document.querySelector(this.container);
    var cardsContainer = container.getElementsByClassName('cards')[0];
    cardsContainer.style.opacity = '0.3';
};

Player.prototype.hideLeftState = function () {
    var container = document.querySelector(this.container);
    var cardsContainer = container.getElementsByClassName('cards')[0];
    cardsContainer.style.opacity = '1';
};

Player.prototype.displayActiveState = function () {
    if (this.type !== 'person') return;
    var container = document.querySelector(this.container);
    var activeContainer = container.getElementsByClassName('active')[0];
    activeContainer.innerHTML = '<img class="active-arrow" src="img/arrow-txt.png">';
};

Player.prototype.hideActiveState = function () {
    if (this.type !== 'person') return;
    var container = document.querySelector(this.container);
    var activeContainer = container.getElementsByClassName('active')[0];
    activeContainer.innerHTML = '';
};

Player.prototype.hasSuit = function (suit) {
    var hasSuit = false;

    this.cards.forEach(function (card) {
        if (card.suit == suit) {
            hasSuit = true;
        }
    });

    return hasSuit;
};

Player.prototype.hasCard = function (card) {
    return this.cards.indexOf(card) !== -1;
};

Player.prototype.reset = function () {
    this.hideBid();
    this.hideLeftState();
    this.isActive = false;
    this.isInitiator = false;
    this.isWaitingPass = false;
    this.isBidAfterTakeOut = false;
    this.isFirstMove = true;
    this.isBidWinner = false;
    this.currentHandsCount = 0;
    this.currentScore = 0;
    this.cards = [];
    this.cardsCopy = [];

    this.currentTrump = {},
    this.goal = 2;
};

Player.prototype.init = function () {
    var template = '', el = document.createElement('div');
    template += '<div class="active"></div>';
    template += '<div class="bids"></div>';
    template += '<div class="text-center">';
    template += '<img class="avatar" src="img/avatars/' + this.name.toLowerCase() + '.png' + '">';
    template += '<p class="name">' + this.name + '</p>';
    template += '</div>';
    template += '<div class="cards"></div>';

    el.id = this.container.substring(1);
    el.className = ' player ' + this.orientation;
    el.innerHTML = template;

    document.body.prepend(el);
};

Player.prototype.willWinGame = function (trumpSuit) {
    var minHands = this.isBidWinner ? 6 : 2;
    var bidScore = SUITS[trumpSuit] * minHands;

    return MAX_SCORE <= this.score + bidScore;

};

Player.prototype.clone = function () {
    var pl = new Player();
    pl.id = this.id;
    pl.name = this.name;
    pl.type = this.type;
    pl.container = this.container;
    pl.orientation = this.orientation;
    pl.isActive = this.isActive;
    pl.isInitiator = this.isInitiator;
    pl.isWaitingPass = this.isWaitingPass;
    pl.isBidAfterTakeOut = this.isBidAfterTakeOut;
    pl.isFirstMove = this.isFirstMove;
    pl.isBidWinner = this.isBidWinner;
    pl.currentHandsCount = this.currentHandsCount;
    pl.score = this.score;
    pl.currentScore = this.currentScore;
    pl.skipCount = this.skipCount;
    pl.cards = this.cards.map(function (card) {
        return card;
    });

    pl.goal = this.goal;

    return pl;
};
function createPlayers() {
    var player = new Player('Student', 'robot', '#student', 'left');
    player.init();
    var player1 = new Player('Master', 'robot', '#master', 'right');
    player1.init();
    var player2 = new Player('User', 'person', '#player', 'middle');
    player2.init();


    return [
        player, player1, player2
    ];
}