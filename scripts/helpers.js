var _find = function (arr, key, value) {
    var result = [];
    arr.forEach(function (o) {
        if (o.hasOwnProperty(key) && o[key] == value)
            result.push(o);
    });

    return result;
};

var intersect = function () {
    return Array.from(arguments).reduce(function (previous, current) {
        return previous.filter(function (element) {
            return current.indexOf(element) > -1;
        });
    });
};

var extend = function () {
    for (var i = 1; i < arguments.length; i++)
        for (var key in arguments[i])
            if (arguments[i].hasOwnProperty(key))
                arguments[0][key] = arguments[i][key];
    return arguments[0];
};

var guid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4();
};

var count = function (arr, el) {
    var count = 0;

    for (var i = arr.length - 1; i >= 0; i--) {
        if (arr[i] === el) count++;
    }

    return count;
};

var delay = function (t) {
    return new Promise(function (resolve) {
        setTimeout(resolve, t)
    });
};


/**
 * NEED REFACTORING
 */
var getLeader = function (players) {
    var leader = players[0];
    players.forEach(function (o) {
        if (o.score >= leader.score)
            leader = o;
    });

    return leader;
};

var getTrump = function (players) {
    var player = _find(players, 'isActive', true)[0];
    var ranking = getSuitsRanking(player.cards);
    console.log(ranking);
    var minTricks = 0;
    var maxTricks = 0;
    var trump = {
        suit: null,
        minTricks: 0,
        maxTricks: 0
    };

    for (var suit in ranking) {
        // TODO:
        minTricks += ranking[suit].minTricks;
        maxTricks += ranking[suit].maxTricks;

        if (ranking[suit].minTricks > trump.minTricks) {
            trump = {
                suit: suit,
                minTricks: ranking[suit].minTricks,
                maxTricks: ranking[suit].maxTricks
            };
        }
    }

    return {
        suit: trump.suit,
        minTricks: minTricks,
        maxTricks: maxTricks,
        details: ranking
    };
};

var updateGoal = function (player, hand) {
    if (player.isBidWinner) {
        var tempGoal = 0;
        for (var suit in player.currentTrump.details) {
            console.log('isSuitCleared', isSuitCleared(suit, player.cards, hand.leftCards, hand.pass), suit);
            console.log('isPossibleToClearSuit', isPossibleToClearSuit(player.cards, hand.leftCards, suit, hand.leftPlayers), suit);
            if (isSuitCleared(suit, player.cards, hand.leftCards, hand.pass) || isPossibleToClearSuit(player.cards, hand.leftCards, suit, hand.leftPlayers)) {
                tempGoal += player.currentTrump.details[suit].minTricks + (getSuitCards(player.cards, suit).length - player.currentTrump.details[suit].minTricks);
            } else {
                tempGoal += player.currentTrump.details[suit].minTricks;
            }
        }

        if(tempGoal > player.goal) {
            return tempGoal;
        }

        return player.goal;
    }

    return player.currentHandsCount + 2;
};

var getSuitsRanking = function (cards) {
    var results = {};

    for (var suit in SUITS) {
        var ranking = possibleRanking[suitToStr(cards, suit)];
        results[suit] = ranking;
    }

    return results;
};

var suitToStr = function (cards, suit) {
    var str = '';
    getSuitCards(cards, suit).forEach(function (card, index) {
        if (index !== 0) {
            str += '-';
        }

        str += card.value;
    });

    return str.trim();
};

var getHiddenCards = function (players, pass, shuffle) {
    var outsideCards = [];//pass;
    var notActivePlayers = _find(players, 'isActive', false);

    notActivePlayers.forEach(function (player) {
        outsideCards = outsideCards.concat(player.cards);
    });

    if (shuffle) {
        outsideCards = shuffleCards(outsideCards);
    }

    return outsideCards;
};

var getCardIndex = function (cards, card) {
    var index = null;
    cards.forEach(function (c, i) {
        if (c.cid === card.cid) {
            index = i;
        }
    });

    return index;
};

var getSuitPossibleHands = function (cards, leftCards, suit, risk) {
    if (!cards.length) return 0;

    cards.sort(function (a, b) {
        return a.value - b.value;
    });

    var outSideCards = getOutSideCards(cards, leftCards, createSuitCards(suit));

    if (!outSideCards.length) {
        return cards.length;
    }

    var tmpCards = cards.slice();
    //console.log('CARDS: ', getCardsStr(tmpCards));
    // console.log('OUTSIDE: ', getCardsStr(outSideCards));

    var handsCount = 0;
    for (var i = tmpCards.length - 1; i >= 0; i--) {
        var winners = false;
        for (var j = outSideCards.length - 1; j >= 0; j--) {
            if (outSideCards[j].value > tmpCards[i].value) {
                winners = true;
                break;
            }
        }

        if (!winners) {
            // console.log('CARD PLAYED: ', tmpCards[i].cid);
            handsCount++;
            outSideCards.splice(getCardIndex(outSideCards, getMinCard(outSideCards)), 1);
            tmpCards.splice(getCardIndex(tmpCards, tmpCards[i]), 1);
        } else {
            var minCard = getMinCard(tmpCards);
            // console.log('CARD PLAYED: ', minCard.cid);
            var minimumOfMaximums = getMinWinner(outSideCards, minCard);
            outSideCards.splice(getCardIndex(outSideCards, minimumOfMaximums), 1);
            tmpCards.splice(getCardIndex(tmpCards, minCard), 1);
        }

        //console.log('CARDS AFTER REMOVE: ', getCardsStr(tmpCards));
        // console.log('OUTSIDE AFTER REMOVE: ', getCardsStr(outSideCards));
        // console.log('HANDS: ', handsCount);

        if (risk) {
            outSideCards.splice(getCardIndex(outSideCards, getMinCard(outSideCards)), 1);
        }
    }

    return handsCount;
};

var getMinCard = function (cards) {
    var min = cards[0];
    for (var i = cards.length - 1; i >= 0; i--) {
        if (min.value > cards[i].value) {
            min = cards[i];
        }
    }

    return min;
};

var getMaxCard = function (cards) {
    var max = cards[0];
    for (var i = cards.length - 1; i >= 0; i--) {
        if (max.value < cards[i].value) {
            max = cards[i];
        }
    }

    return max;
};

var getMinWinner = function (cards, card) {
    var max = false;

    for (var i = cards.length - 1; i >= 0; i--) {
        if (card.value < cards[i].value) {
            if (!max) {
                max = cards[i];
            } else if (max.value > cards[i].value) {
                max = cards[i];
            }
        }
    }
    return max ? max : false;
};

var suitExists = function (cards, suit) {
    var isExists = false;

    cards.forEach(function (card) {
        if (card.suit == suit) {
            isExists = true;
        }
    });

    return isExists;
};

var getSuitCards = function (cards, suit) {
    var c = [];

    cards.forEach(function (card) {
        if (card.suit == suit) {
            c.push(card);
        }
    });

    return c;
};

var getOutSideCards = function (playerCards, leftCards, allCards) {
    var outside = [];
    allCards.map(function (card) {
        var isPlayerHasCard = false;
        for (var i = playerCards.length - 1; i >= 0; i--) {
            if (playerCards[i].cid === card.cid) {
                isPlayerHasCard = true;
            }
        }

        var isLeftCard = false;
        for (var j = leftCards.length - 1; j >= 0; j--) {
            if (leftCards[j].cid === card.cid) {
                isLeftCard = true;
            }
        }

        if (!isPlayerHasCard && !isLeftCard) {
            outside.push(card);
        }
    });

    return outside;
};

var getWinnerCard = function (cards, hand) {
    for (var suit in SUITS) {
        if (suit !== hand.trumpSuit) {
            var suitCards = getSuitCards(cards, suit);
            if (suitCards.length) {
                if (!cards.length) continue;

                var maxCard = getMaxCard(suitCards);
                var outSideCards = getOutSideCards(suitCards, hand.leftCards, createSuitCards(suit));

                if (outSideCards.length) {
                    var maxOutSide = getMaxCard(outSideCards);
                    if (maxCard.value > maxOutSide.value) {
                        return maxCard;
                    } else {
                        continue;
                    }
                }
                return maxCard;
            }
        }
    }

    return false;
};

var isPossibleToClearSuit = function (cards, leftCards, suit, leftPlayers) {
    var suitCards = getSuitCards(cards, suit);
    if (leftPlayers.length) {
        var leftPlayersCards = getSuitCards(leftPlayers[0].cards, suit);
        leftCards.concat(leftPlayersCards);
    }

    if (suitCards.length) {
        var hands = getSuitPossibleHands(suitCards, leftCards, suit, false);
        var outSideCards = getOutSideCards(suitCards, leftCards, createSuitCards(suit));
        if (suitCards.length < outSideCards.length) {
            return false;
        }

        if (hands < outSideCards.length) {
            return false;
        }

        var maxSuit = getMaxCard(suitCards);
        for (var i = outSideCards.length - 1; i >= 0; i--) {
            if (maxSuit.value < outSideCards[i].value) {
                return false;
            }
        }

    } else { //TODO: remove
        return false;
    }

    return true;
};

var getSuitPossibleHands = function (cards, leftCards, suit, risk) {
    if (!cards.length) return 0;

    cards.sort(function (a, b) {
        return a.value - b.value;
    });

    var outSideCards = getOutSideCards(cards, leftCards, createSuitCards(suit));

    if (!outSideCards.length) {
        return cards.length;
    }

    var handsCount = 0;
    for (var i = cards.length - 1; i >= 0; i--) {
        var winners = false;
        for (var j = outSideCards.length - 1; j >= 0; j--) {
            if (outSideCards[j].value > cards[i].value) {
                winners = true;
                break;
            }
        }

        if (!winners) {
            handsCount++;
        }

        outSideCards.splice(outSideCards.indexOf(getMinCard(outSideCards)));

        if (risk) {
            outSideCards.splice(outSideCards.indexOf(getMinCard(outSideCards)));
        }
    }

    return handsCount;
};
/** USEFULL FUNCTINOS FOR TACKTIC**/
var isSuitCleared = function (suit) {
    var sum = 0;
    for (var i = arguments.length - 1; i >= 1; i--) {
        for (var j = arguments[i].length - 1; j >= 0; j--) {
            if (arguments[i][j].suit === suit) {
                sum++;
            }
        }
    }

    return (sum === 8);
};

var isCardExist = function(value, suit, cards) {
    var isExist = false;
    cards.forEach(function(card){
        if(card.suit === suit && card.value === value) {
            isExist = true;
        }
    });

    return isExist;
};

