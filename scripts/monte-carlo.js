var PastonState = function (players, hand) {
    this.hand = hand;
    this.players = players;
    this.cardsOnTable = [];
    this.tricksTaken = {
        0: 0,
        1: 0,
        2: 0
    };
};
PastonState.prototype.CloneAndRandomize = function (randomize) {
    // Shuffle cards
    var outsideCards = getHiddenCards(this.players, this.hand.pass, true);
    var st = new PastonState(this.players);
    st.hand = this.hand;
    st.trumpSuit = this.hand.trumpSuit;
    st.activePlayerIndex = this.players.indexOf(_find(this.players, 'isActive', true)[0]);
    st.originalPlayerIndex = this.players.indexOf(_find(this.players, 'isActive', true)[0]);

    for (var j = 0; j < this.hand.currentStack.length; j++) {
        for (var k = st.players.length - 1; k >= 0; k--) {
            if (this.hand.currentStack[j].player.id === st.players[k].id) {
                st.cardsOnTable.push({
                    card: this.hand.currentStack[j].card,
                    playerIndex: k
                });
            }
        }
    }

    st.players = this.players.map(function (player) {
        var pl = player.clone();
        if (randomize && !pl.isActive) {
            // Get some info from previous hands
            // If player didn`t have suit
            var suitsMissing = [];
            for (var k = 0; k < st.hand.previousHands.length; k += st.players.length) {
                var activeSuit = st.hand.previousHands[k].card.suit;
                var childHand;
                for (var t = k; t < k + st.players.length; t++) {
                    if (st.hand.previousHands[t].player.id === pl.id) {
                        childHand = st.hand.previousHands[t];
                    }
                }

                if (childHand.card.suit !== activeSuit) {
                    if (suitsMissing.indexOf(activeSuit) === -1) {
                        suitsMissing.push(activeSuit);
                    }
                }
            }

            var randomCards = [];
            for (var p = 0; p < outsideCards.length; p++) {
                if (randomCards.length === pl.cards.length) break;

                if (suitsMissing.indexOf(outsideCards[p].suit) === -1) {
                    randomCards.push(outsideCards[p]);
                }
            }
            pl.cards = randomCards;

            // pl.cards = outsideCards.splice(0, pl.cards.length);

            // Just for console log, should be removed
            var newCards = [];
            pl.cards.forEach(function (card) {
                newCards.push(card.cid);
            });
            //if(pl.cards.length === 2) {
            //    console.log(pl.name, ' NEW CARDS: ', newCards.join(', '));
            //}

        }

        return pl;
    });

    return st;
};
PastonState.prototype.DoMove = function (move) {
    this.cardsOnTable.push({
        card: move,
        playerIndex: this.activePlayerIndex
    });

    var index = this.players[this.activePlayerIndex].cards.indexOf(move);
    this.players[this.activePlayerIndex].cards.splice(index, 1);

    if (this.cardsOnTable.length === 3) {
        var winner = this.cardsOnTable[0];

        for (var i = this.cardsOnTable.length - 1; i >= 0; i--) {
            if (this.cardsOnTable[i].card.suit == winner.card.suit) {
                if (this.cardsOnTable[i].card.value > winner.card.value) {
                    winner = this.cardsOnTable[i];
                }
            } else {
                if (this.cardsOnTable[i].card.suit == this.trumpSuit) {
                    winner = this.cardsOnTable[i];
                }
            }
        }
        //console.log("%c WINNER IS: " + this.players[winner.playerIndex].name, PRIMARY);
        this.activePlayerIndex = winner.playerIndex;
        this.tricksTaken[winner.playerIndex] += 1;
        this.cardsOnTable = [];
    } else {
        this.activePlayerIndex = (typeof this.players[++this.activePlayerIndex] !== 'undefined') ? this.activePlayerIndex : 0;
    }
};
PastonState.prototype.getTableCards = function () {
    var cards = [];
    this.cardsOnTable.forEach(function (c) {
        cards.push(c.card.cid);
    });

    return cards;
};
PastonState.prototype.GetMoves = function () {
    var moves = this.players[this.activePlayerIndex].cards;
    if (this.cardsOnTable.length) {
        this.activeSuit = this.cardsOnTable[0].card.suit;
        if (suitExists(moves, this.activeSuit)) {
            moves = getSuitCards(moves, this.activeSuit)
        } else if (suitExists(moves, this.trumpSuit)) {
            moves = getSuitCards(moves, this.trumpSuit);
        }
    }

    return moves;
};

var Node = function (move, parent) {
    this.move = move; // the move that got us to this node - "Null" for the root node
    this.parentNode = parent; // "Null" for the root node
    this.childNodes = [];
    this.wins = 0;
    this.score = 0;
    this.tricks = 0;
    this.visits = 0;
    this.avails = 1;
};
Node.prototype.Update = function (state) {

    var player = state.players[state.originalPlayerIndex];
    var tricks = state.players[state.originalPlayerIndex].currentHandsCount + state.tricksTaken[state.originalPlayerIndex];

    if(player.isBidWinner) {
        if (state.activePlayerIndex) {
            this.wins += (tricks == player.goal);
        }
    } else {
        var bidWinner = _find(state.players, 'isBidWinner', true)[0];
        var bidWinnerIndex = state.players.indexOf(bidWinner);
        var bidWinnerTricks = state.players[bidWinnerIndex].currentHandsCount + state.tricksTaken[bidWinnerIndex];
        if(state.players[state.originalPlayerIndex].currentHandsCount >= player.goal) {
            this.wins += bidWinnerTricks <= 5;
        } else {
            this.wins += (tricks == player.goal);
        }
    }

    this.visits += 1;
};
Node.prototype.GetUntriedMoves = function (legalMoves) {
    //Return the elements of legalMoves for which this node does not have children.
    var results = [];
    // Find all moves for which this node *does* have children
    var triedMoves = this.childNodes.map(function (child) {
        return child.move;
    });

    legalMoves.forEach(function (move) {
        if (triedMoves.indexOf(move) === -1) {
            results.push(move);
        }
    });

    // Return all moves that are legal but have not been tried yet
    return results;
};
Node.prototype.UCBSelectChild = function (legalMoves) {
    var exploration = 7; //constant
    /* Use the UCB1 formula to select a child node, filtered by the given list of legal moves.
     exploration is a constant balancing between exploitation and exploration, with default value 0.7 (approximately sqrt(2) / 2)
     */

    // Filter the list of children by the list of legal moves
    var legalChildren = this.childNodes.filter(function (child) {
        if (legalMoves.indexOf(child.move) !== -1) {
            return child;
        }
    });

    // Get the child with the highest UCB score
    // var s = max(legalChildren, key = lambda c: float(c.wins)/float(c.visits) + exploration * sqrt(log(c.avails)/float(c.visits)))
    var s = {
        node: legalChildren[0],
        UCB: 0
    };

    //console.log('CHILDS: ', legalChildren);
    legalChildren.forEach(function (c) {
        // UCB 1
        //var UCB = c.wins/c.visits + exploration * Math.sqrt(Math.log(c.avails) / c.visits);
        //var UCB = c.score + exploration * Math.sqrt(Math.log(c.avails) / c.visits);

        // UCB 1 using RAVE
        var UCB = c.wins / c.visits + exploration * Math.sqrt(Math.log(c.avails) / c.visits);

        //console.log('WINS: ', c.wins, ' VISITS: ', c.visits);
        if (c.move) {
            // //console.log(c.move.cid, UCB);
        }

        if (UCB > s.UCB) {
            s = {
                node: c,
                UCB: UCB
            };
        }
    });


    if (s.node.move) {
        //console.log("MAX USB", s.UCB, s.node.move.cid);
    }

    // Update availability counts -- it is easier to do this now than during backpropagation
    for (var i = 0; i < legalChildren.length; i++) {
        legalChildren[i].avails++;
    }

    // Return the child selected above
    return s.node;
};
Node.prototype.AddChild = function (m, p) {
    /* Add a new child node for the move m.
     Return the added child node
     */
    var n = new Node(m, this, p);
    this.childNodes.push(n);
    return n;
};
Node.prototype.treeToString = function (indent) {
    var cid = this.move ? this.move.cid : 'ROOT ';
    var s = this.indentString(indent) + cid;
    for (var key in this) {
        if (['object', 'function'].indexOf(typeof this[key]) === -1) {
            s += '  ' + key.toUpperCase() + ': ' + this[key];
        }
    }

    this.childNodes.forEach(function (c) {
        s += c.treeToString(indent + 1);
    });

    return s;
};
Node.prototype.indentString = function (indent) {
    var s = "\n";
    for (var i = 0; i <= indent + 1; i++) {
        s += "| ";
    }

    return s;
};
Node.prototype.childrenToString = function () {
    var s = "ROOT VISITS: " + this.visits + "\n";
    this.childNodes.forEach(function (c) {
        s += c.move.cid;
        for (var key in c) {
            if (['object', 'function'].indexOf(typeof c[key]) === -1) {
                s += '  ' + key.toUpperCase() + ': ' + c[key];
            }
        }
        s += "\n";
    });

    return s;
};

var ISMCTS = function (rootState, itermax, verbose) {
    var rootNode = new Node(null, null);

    for (var i = 0; i < itermax; i++) {
        //console.log(safeHands, riskyHands, goal);
        var node = rootNode;

        // DETERMINIZE
        var state = rootState.CloneAndRandomize(false);

        //console.log('%c ACTIVE PLAYER IS: ' + state.players[state.activePlayerIndex].name, SUCCESS);
        //console.log('%c CARDS ON TABLE: ' + state.getTableCards().join(), SUCCESS);
        // SELECT
        //console.log('%c ///SELECT LOOP = ' + (state.GetMoves().length && !node.GetUntriedMoves(state.GetMoves()).length) + '/// GET MOVES, UNTRIEDMOVES', BLACK, state.GetMoves(), node.GetUntriedMoves(state.GetMoves()));
        while (state.GetMoves().length && !node.GetUntriedMoves(state.GetMoves()).length) {
            // node is fully expanded and non-terminal
            node = node.UCBSelectChild(state.GetMoves());
            //console.log('%c  NODE SELECTED: ', ERROR, node);
            //console.log('%c  DO MOVE IN SELECT: ', ERROR, node.move);
            state.DoMove(node.move);
            //console.log('%c CARDS ON TABLE: ' + state.getTableCards().join(), SUCCESS);
        }

        // EXPAND
        var untriedMoves = node.GetUntriedMoves(state.GetMoves());
        //console.log('%c EXPAND AS UNTRIED MOVES= ', BLACK, untriedMoves);
        if (untriedMoves.length) { // if we can expand (i.e. state/node is non-terminal)
            var m = untriedMoves[Math.floor(Math.random() * untriedMoves.length)];
            var player = state.activePlayerIndex;
            //console.log('%c MOVE IN EXPAND: ' + state.players[state.activePlayerIndex].name + ': ', PRIMARY, m);
            state.DoMove(m);
            //console.log('%c CARDS ON TABLE: ' + state.getTableCards().join(), SUCCESS);
            node = node.AddChild(m, player); // add child and descend tree
            //console.log('%c NEW NODE CREATED: ', WARNING, node);
        }

        // SIMULATE
        //console.log('%c START SIMULATION AS MOVES =', PRIMARY, state.GetMoves());
        state.cardsOnTable.forEach(function (c) {
            //console.log('%c SIMULATED MOVE: ' + state.players[c.playerIndex].name + ': ', INFO, c.card);
        });
        while (state.GetMoves().length) { // while state is non-terminal
            var moves = state.GetMoves();
            var move = moves[Math.floor(Math.random() * moves.length)];
            //console.log('%c SIMULATED MOVE: ' + state.players[state.activePlayerIndex].name + ': ', INFO, move);
            state.DoMove(move);
        }


        // BACKPROPAGATE
        while (node != null) {
            // backpropagate from the expanded node and work back to the root node
            node.Update(state);
            node = node.parentNode;
        }
    }

    if (verbose) {
        //console.log(rootNode.treeToString(0));
        // console.log(rootNode.childrenToString());
    }
    else {
        ////console.log(rootNode.childrenToString());
    }

    //console.log(rootNode);
    var bestMove = {
        visits: 0,
        card: null
    };

    rootNode.childNodes.forEach(function (node) {
        if (node.visits > bestMove.visits) {
            bestMove = {
                visits: node.visits,
                card: node.move,
                rootWins: rootNode.wins,
                rootVisits: rootNode.visits
            };
        }
    });

    return bestMove;
};
