'use strict';

var Deck = function () {

    var shuffle = function () {
        this.cards = shuffleCards(this.cards);

        return this;
    };

    return {
        cards: createDeck(),
        shuffle: shuffle
    };
};

var shuffleCards = function (cards) {
    var cards = sortDeck(cards);

    var currentIndex = cards.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = cards[currentIndex];
        cards[currentIndex] = cards[randomIndex];
        cards[randomIndex] = temporaryValue;
    }

    return cards;
};


var createDeck = function(){
    var deck = [];
    for (var suit in SUITS) {
        Array.prototype.push.apply(deck, createSuitCards(suit));
    }

    return deck;
};
var createSuitCards = function(suit){
    var cards = [];

    for (var j = CARD_NAMES.length - 1; j >= 0; j--) {
        cards.push(new Card(j + 7, CARD_NAMES[j], suit));
    }

    return cards;
};

var sortDeck = function(cards){
    return cards.sort(function(a, b){
        if (a.suit === b.suit) {
            return ((a.value < b.value) ? -1 : (a.value > b.value) ? 1 : 0) * -1;
        } else {
            return ((SUITS[a.suit] < SUITS[b.suit]) ? -1 : (SUITS[a.suit] > SUITS[b.suit]) ? 1 : 0) * -1;
        }
    });
};