var Widgets = function () {

    /**
     * Construct
     */
    (function () {
        var template = '', board, trump;

        template += '<div id="left-card"></div>';
        template += '<div id="right-card"></div>';
        template += '<div id="middle-card"></div>';

        board = document.createElement('div')
        board.id = 'board';
        board.innerHTML = template;

        trump = document.createElement('div')
        trump.id = 'trump';

        document.body.appendChild(board);
        document.body.appendChild(trump);
    })();

    var updateHandCounts = function (players) {
        if (document.getElementById('hand')) {
            document.getElementById('hand').remove();
        }

        var container = document.createElement('div');
        container.id = 'hand';
        container.innerHTML = '<p>HANDS COUNT</p>';

        players.map(function (player) {
            var playerContainer = document.createElement('div');
            var countContainer = document.createElement('div');
            countContainer.className = 'count';
            countContainer.innerHTML = player.currentHandsCount;

            if (player.orientation !== 'middle') {
                playerContainer.className = 'pull-left';
                playerContainer.style.width = '50%';
            } else {
                var clearFix = document.createElement('div');
                clearFix.className = 'clearfix';
                container.appendChild(clearFix);
            }

            playerContainer.appendChild(countContainer);
            container.appendChild(playerContainer);
        });

        document.body.appendChild(container);
    };

    var updateScores = function (players) {
        if (document.getElementById('score')) {
            document.getElementById('score').remove();
        }

        var score = document.createElement('div');
        score.id = 'score';

        var table = document.createElement('table');
        var head = document.createElement('tr');
        for (var e = 0; e < 3; e++) {
            var td = document.createElement('td');
            td.innerHTML = players[e].name;
            head.appendChild(td);
        }
        table.appendChild(head);

        var body = document.createElement('tr');
        for (var i = 0; i < 3; i++) {
            var scoreTxt = document.createElement('td');
            scoreTxt.innerHTML = players[i].score;
            body.appendChild(scoreTxt);
        }

        table.appendChild(body);
        score.appendChild(table);

        document.body.appendChild(score);
    };

    var displayGameOver = function(player){  
        var el = document.getElementById('results');
        if (el) {
            el.remove();
        }

        var board = document.getElementById('board');
        if (board) {         
            board.remove(); 
        }

        var container = document.createElement('div')
        container.id = 'results';

        var title = document.createElement('h3');
        title.className = 'text-center';
        title.innerHTML = '<strong>' + player.name + ' WIN THE GAME</strong>';

        var newGame = document.createElement('button');
            newGame.className = 'newGame';
            newGame.innerHTML = "Start New Game";
            newGame.onclick  = function(){
                location.reload();
            };
        container.appendChild(title);
        container.appendChild(newGame);
     
        document.body.appendChild(container);
    };

    var displayResults = function (players, hand) {
        if (!hand) return;

        var el = document.getElementById('results');
        if (el) {
            el.remove();
        }

        var container = document.createElement('div')
        container.id = 'results';

        var title = document.createElement('h3');
        title.className = 'text-center';

        var bidEl;
        if (hand.currentBid) {
            bidEl = document.createElement('img');
            bidEl.src = 'img/' + hand.currentBid.toLowerCase() + '.png';
        } else {
            bidEl = document.createElement('span');
            bidEl.innerHTML = '<strong>SKIP HAND</strong>';
        }

        title.appendChild(bidEl);
        container.appendChild(title);

        players.map(function (player) {
            var p = document.createElement('p');
            var oldScore = player.score - player.currentScore;
            var operator = '+';
            if (player.currentScore < 0) operator = '';
            p.innerHTML = player.name + ': ' + '<strong>' + operator + player.currentScore + '</strong>';

            if (player.isBidWinner) {
                title.prepend(player.name + ' ');
            }

            container.appendChild(p);

        });
        document.body.appendChild(container);
    };

    var hideResults = function () {
        var results = document.getElementById('results');
        if (results)
            results.remove();
    };

    var clearCards = function () {
        var lastHandCards = document.getElementById('history');
        var currentHandCards = document.querySelectorAll('#board img');
        if(!currentHandCards.length) {
            return;
        }
        var txt = document.createElement('p');
        txt.innerHTML = 'LAST HAND';

        if (!lastHandCards) {
            lastHandCards = document.createElement('div');
            lastHandCards.id = 'history';
            document.body.appendChild(lastHandCards);
        }

        lastHandCards.innerHTML = '';
        lastHandCards.appendChild(txt);
        currentHandCards.forEach(function (img) {
            img.style.width = '50px';
            lastHandCards.appendChild(img);
        });
    };

    return {
        updateHandCounts: updateHandCounts,
        updateScores: updateScores,
        displayResults: displayResults,
        displayGameOver:displayGameOver,
        hideResults: hideResults,
        clearCards: clearCards
    };
};