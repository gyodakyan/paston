'use strict';

var Card = function (value, name, suit) {

    var move = function (orientation) {
        var board = document.getElementById(orientation + '-card');
        if (!board) return;

        board.append(this.img);
        this.showFace();
    };

    var remove = function () {
        if (this.img) this.img.remove();
    };

    var showFace = function () {
        this.img.src = this.options.imagesUrl + this.name + this.suit[0] + '.svg';
    };

    var draw = function (container, face, w) {
        var width = w || this.options.width;
        var src = this.options.imagesUrl + this.name + this.suit[0] + '.svg';

        if (!face) {
            src = this.options.imagesUrl + 'Red_Back.svg';
        }

        var img = document.createElement('img');
        img.width = width;
        img.className = 'card';
        img.cid = this.id;
        img.src = src;
        this.img = img;

        container.appendChild(img);
    };

    return {
        cid: name + suit[0],
        value: value,
        name: name,
        suit: suit,
        options: {
            width: 50,
            imagesUrl: 'img/cards/'
        },
        move: move,
        remove: remove,
        draw: draw,
        showFace: showFace
    };
};
